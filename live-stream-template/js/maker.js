﻿
var data;
var month_list = ["Enero", "Febrero", "Marzo", "Abril", "Mayo",
					"Junio", "Julio", "Agosto", "Septiembre", "Octubre", 
					"Noviembre", "Diciembre"];

var b = '\n', t = '\t', n = '--';

var theme, // aka title
theme_nq,
escuela_num = "",
service_type = "",
type_of_day = "",
date,
month,
day,
year,
honorific,
honorific2,
speaker = "",
singer = "",
videoURL,
embededURL;

var widget,
widget_desc,
hoa, 
event,
sermon,
sermon_body,
sermon_url,
facebook,
facebook_live,
anchor;

$(document).ready(function () {
 	$('[data-toggle="tooltip"]').tooltip();

 	// when other is selected, open a textbox
	document.getElementById("speaker").onchange = function(event){
		speaker = $('#speaker option:selected').text();
		// open textbox
		if (speaker == "Other") {
			$("#other").show();
		} else {
			$("#other").hide();
		}
	}

	document.getElementById("serviceType").onchange = function(event){
	    service_type = $('#serviceType option:selected').text();
		// open textbox when other service is selceted
		if (service_type == "Other") {
			$("#serviceTypeOther").show();
		} else {
			$("#serviceTypeOther").hide();
		}

		// when escuela is selected, open a textbox
		if (service_type == "Escuela Sabática #") {
			$("#escuelaNumber").show();
		} else {
			$("#escuelaNumber").hide();
			$("#escuelaNumber").val('01');
			escuela_num = "";
		}
	}

	$("button").click(function (event) {
		if ($(this).attr("id") == "generate") {
			
			// get typed content
			theme = $('#theme').val();

			// get dropdown content
			speaker = $('#speaker option:selected').text();
			honorific = $('#honorific option:selected').text();
			service_type = $('#serviceType option:selected').text();

			switch ($('#serviceType option:selected').val()) {
				case '01':
					type_of_day = "Servicio Matutino";
					break;
				case '02':
					type_of_day = "Servicio Vespertino";
					break;
				default:
					type_of_day = service_type;
					break;
			}

			if (service_type == "Other") {
				service_type = $('#serviceTypeOther').val();
			} else if (service_type == "Escuela Sabática #") {
				escuela_num = $('#escuelaNumber option:selected').text();
			}
			theme_nq = theme;
			theme = '"' + theme + '"';
			
			if (speaker == "Other") {
				speaker = $('#other').val();
			}


			
			sermon_url = $('#sermonURL').val();
			if(sermon_url == '') {
				sermon_url = '[Add Sermon URL]';
			}

			// month, day and year
			date = new Date();
			
			month_num = date.getMonth() + 1;
			month = month_list[month_num-1];
			day = date.getDate();
			year = date.getFullYear(); 


			// common stuff put together
			joined_date = day + ' de ' + month + ' ' + year;
			joined_title = service_type + escuela_num + ': ' + theme;
			joined_speaker = honorific + ' ' + speaker;
			
			// youtube embeded and URL
			videoURL = $('#videoURL').val();
			embededURL = $('#embededURL').val();

			// Debugging
			/*alert('data: ' + theme + ' | ' + service_type + ' | ' 
			 	+ month + ' | ' + day  + ' | ' + speaker  + ' | ' + 
			 	videoURL + ' | ' + embededURL+ ' | ' + sermon_url);*/
			
			
			// widget
			widget = n + 'Widget' + n + 
				b + 'Title: ' + b + joined_title + b +
				b + 'Description: ' + b + joined_speaker + ' ' + joined_date +  b +
				b + 'Video URL: ' + b + videoURL;
			
			// YouTube Live Events
			hoa = n + 'YouTube Live Events'+ n + 
				b +'Title: ' + b + joined_title + ' — ' + joined_date;
			
			// events
			event =  n + 'Events' + n + 
				b + 'Title: ' + b + joined_title +
				b + 
				b + 'Body: ' + 
				b + '<h2>Todos los Sabados</h2>' + 
				b + joined_title + 
				b + honorific + ' '  + speaker + 
				b + 'Sábado ' + joined_date +
				b + embededURL;
			
			// sermon
			sermon =  n + 'Sermon' + n + 
				b + 'Title: ' + b + joined_title + 
				b + 
				b + 'Body: ' + 
				b + joined_title + 
				b + joined_speaker + 
				b + 'Sábado ' + joined_date +
				b + 
				b + 'Video: ' + b + videoURL;
			
			// Facebook
			facebook = n + 'Facebook' + n + 
				b + 'En Vivo! ' + service_type + escuela_num  + 
				b + 'Tema: ' + theme + 
				b + 'Expositor: ' + honorific + ' '  + speaker + 
				b + sermon_url;

			// Facebook Live
			var alt_title = theme;
			// change the alternative title for afternoon or other service 
			if($('#serviceType option:selected').val() == '02' ||
				$('#serviceType option:selected').val() == '03') {
				alt_title = 'Tema: ' + alt_title;
			} else {
				alt_title = joined_title;
			}

			facebook_live = n + 'Facebook Live' + n + 
				b + 'EN VIVO ' + type_of_day +
				b + 'Predicador: ' + joined_speaker +
				b + alt_title; /*+ b + TODO make this in the form...
				b + 'Alabanza y Adoracion: ' + honorific2 + ' ' + singer;*/

			anchor = n + 'Anchor' + n + 
				b + 'Title: ' + b + theme_nq + 
				b + 
				b + 'Body: ' + 
				b + sermon_url + b + 
				b + joined_title + 
				b + joined_speaker + 
				b + 'Sábado ' + joined_date;

		
			data = hoa + b + b + facebook_live + b + b + sermon + b + b + 
					facebook + b +  b + widget + b + b + anchor; // + b + b + event;
			
			// display results
			$("#output").val(data);
		} else if ($(this).attr("id") == "reset") {
			// clear everything
			data = "";

			$('#theme').val('');
			$('#videoURL').val('');
			$('#embededURL').val('');
			$('#sermonURL').val('');

			$('#serviceTypeOther').val('');
			$('#other').val('');
			$('#speaker').val('');
			$('#serviceType').val('');
			
			$("#other").val('');
			$("#serviceTypeOther").val('');
			$("#escuelaNumber").val('01');

			$("#other").hide();
			$("#serviceTypeOther").hide();
			$("#escuelaNumber").hide();

			$("#output").val(data);
		}
  	});//end of button.click
});//end of doc.ready
